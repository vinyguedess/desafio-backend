import os
import json
import unittest
from src.config.database import db
from src.models import User
from src import app


class TestAuth(unittest.TestCase):

    def setUp(self):
        self.test_user = User(name="Darth Vader", email="darth.vader@gmail.com", password="0987654321")
        db.session.add(self.test_user)
        db.session.commit()

        self.app = app.test_client()
        self.app.testing = True

    def test_user_signup(self):
        rv = self.app.post("/auth/sign_up", json={
            "name": "Luke Skywalker",
            "email": "luke.not.vader@gmail.com",
            "pass": "@123@",
            "phones": [
                { "number": "98760-1233", "code": "11" }
            ]
        })
        data = json.loads(rv.data)

        assert "access_token" in data
        assert "refresh_token" in data
        assert rv.status_code == 201

    def test_user_signup_error_when_email_already_registered(self):
        rv = self.app.post("/auth/sign_up", json={
            "name": "Darth Vader",
            "email": "darth.vader@gmail.com",
            "pass": "@123@",
            "phones": [
                { "number": "98760-1233", "code": "11" }
            ]
        })
        data = json.loads(rv.data)

        assert "message" in data
        assert rv.status_code == 403

    def test_login_success(self):
        rv = self.app.post("/auth/login", json={
            "email": "darth.vader@gmail.com",
            "pass": "0987654321"
        })
        data = json.loads(rv.data)

        assert "access_token" in data
        assert "refresh_token" in data
        assert rv.status_code == 200

    def test_login_failed_with_wrong_email(self):
        rv = self.app.post("/auth/login", json={
            "email": "non_existent",
            "pass": "wrong_pass"
        })

        assert rv.status_code == 403

    def test_login_failed_with_wrong_password(self):
        rv = self.app.post("/auth/login", json={
            "email": "darth.vader@gmail.com",
            "pass": "wrong_pass"
        })

        assert rv.status_code == 401

    def tearDown(self):
        User.query.delete()


if __name__ == "__main__":
    unittest.main()
