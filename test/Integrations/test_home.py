import os
import json
import unittest
from src import app


class TestHome(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_home(self):
        rv = self.app.get("/")
        data = json.loads(rv.data)

        assert rv.status_code == 200
        assert data["status"] is True


if __name__ == "__main__":
    unittest.main()
