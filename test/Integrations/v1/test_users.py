import unittest
from src.models import User
from src.config.database import db
from src import app


class TestUsers(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        
        self.test_user = User(name="Tem Bici", email="tembici@gmail.com", password="tembici@321")
        db.session.add(self.test_user)

        self.another_user = User(name="Other Tem Bici", email="o_tembici@gmail.com", password="tembici@321")
        db.session.add(self.another_user)

        db.session.commit()

    def test_fetch_all_users(self):
        with app.app_context():
            rv = self.app.get("/v1/users", headers={
                "Authorization": "Bearer %s" % self.test_user.tokens["access_token"]
            })

            assert rv.status_code == 200

    def test_fetch_all_users_error_if_missing_authentication(self):
        with app.app_context():
            rv = self.app.get("/v1/users")

            assert rv.status_code == 401

    def test_fetch_user_by_id(self):
        with app.app_context():
            rv = self.app.get("/v1/users/%s" % self.test_user.id, headers={
                "Authorization": "Bearer %s" % self.test_user.tokens["access_token"]
            })

            rv.status_code = 200

    def test_fetch_user_by_id_error_if_non_existent_user(self):
        with app.app_context():
            rv = self.app.get("/v1/users/%s" % -20, headers={
                "Authorization": "Bearer %s" % self.test_user.tokens["access_token"]
            })

            rv.status_code = 404

    def test_fetch_user_by_id_error_if_not_own_user(self):
        with app.app_context():
            rv = self.app.get("/v1/users/%s" % self.another_user.id, headers={
                "Authorization": "Bearer %s" % self.test_user.tokens["access_token"]
            })

            rv.status_code = 403

    def tearDown(self):
        User.query.delete()


if __name__ == "__main__":
    unittest.main()