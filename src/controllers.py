import hashlib
from flask import jsonify
from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
from src.config.database import db
from src.models import User, Phone


class HomeStatus(Resource):
    def get(self):
        return {"status": True, "message": "API is live"}


class AuthSignUp(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name", type=str, help="User name is required")
        parser.add_argument("email", type=str, help="User e-mail is required")
        parser.add_argument("pass", type=str, help="User password is required")
        parser.add_argument("phones", type=dict, action="append")
        
        data = parser.parse_args()
        user = User.query.filter_by(email=data["email"]).first()
        if isinstance(user, User):
            return { "message": "User already exists" }, 403
        
        user = User(name=data["name"], email=data["email"], password=data["pass"])
        [ user.phones.append(Phone(number=p["number"], code=p["code"])) for p in data["phones"] ]
        
        db.session.add(user)

        db.session.commit()
        return { **user.serialize, **user.tokens }, 201


class AuthLogin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("email", type=str, help="User e-mail is required")
        parser.add_argument("pass", type=str, help="User password is required")

        data = parser.parse_args()
        user = User.query.filter_by(email=data["email"]).first()
        if not isinstance(user, User):
            return { "message": "E-mail or password is wrong" }, 403

        hasher = hashlib.sha256()
        hasher.update(str.encode(data["pass"]))
        if user.password != hasher.hexdigest():
            return { "message": "Login or password is wrong" }, 401

        return user.tokens
            
            
        
        