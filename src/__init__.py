from flask import jsonify
from flask_restful import Api
from src.app import app
from src.config.auth import jwt
from src.config.database import db
import src.controllers as core
import src.v1.controllers as v1


db.create_all()

api = Api(app)
api.add_resource(core.HomeStatus, "/")
api.add_resource(core.AuthSignUp, "/auth/sign_up")
api.add_resource(core.AuthLogin, "/auth/login")

api.add_resource(v1.UsersFetch, "/v1/users")
api.add_resource(v1.UsersFetchById, "/v1/users/<id>")

jwt._set_error_handler_callbacks(app)