
database = {
    "driver": "sqlite",
    "connectionString": 'sqlite:///../tembici.sqlite'
}

security = {
    "jwt": {
        "algorithm": "HS256",
        "secret": "my_s3cr3t_k3y_for_s3cur3_c4ll5_0n_my_4p1"
    }
}