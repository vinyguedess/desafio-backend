from flask_jwt_extended import JWTManager
from src.app import app
from src.config import security


app.config["JWT_SECRET_KEY"] = security.get("jwt").get("secret")
app.config['PROPAGATE_EXCEPTIONS'] = True

jwt = JWTManager(app)