from flask_sqlalchemy import SQLAlchemy
from src.app import app
from src.config import database


app.config['SQLALCHEMY_DATABASE_URI'] = database["connectionString"]
db = SQLAlchemy(app)