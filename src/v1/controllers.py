from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_raw_jwt
from src.models import User


class UsersFetch(Resource):

    @jwt_required
    def get(self):

        users = User.query.all()
        return [u.serialize for u in users], 200


class UsersFetchById(Resource):

    @jwt_required
    def get(self, id):
        user = User.query.filter_by(id=id).first()
        if not isinstance(user, User):
            return { "message": "User does not exist" }, 404

        payload = get_raw_jwt()
        if str(payload["identity"]) != id:
            return { "message": "Not authorized" }, 403

        return user.serialize