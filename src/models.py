import hashlib
import datetime
from flask_jwt_extended import create_access_token, create_refresh_token
from sqlalchemy import Column, ForeignKey, String, Integer, DateTime, event
from src.config.database import db
from src.config import security


class User(db.Model):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    password = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.now)
    updated_at = Column(DateTime, onupdate=datetime.datetime.now, nullable=True)

    @property
    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "phones": [ phone.serialize for phone in self.phones ],
            "created_at": self.created_at.__str__(),
            "updated_at": self.updated_at.__str__()
        }
    
    @property
    def tokens(self):
        return {
            "access_token": create_access_token(identity=self.id),
            "refresh_token": create_refresh_token(identity=self.id)
        }


class Phone(db.Model):
    __tablename__ = "phones"
    id = Column("id", Integer, primary_key=True)
    user_id = Column("user_id", None, ForeignKey("users.id"))
    number = Column("number", String)
    code = Column("code", String)

    user = db.relationship("User", backref=db.backref("phones", lazy=True))

    @property
    def serialize(self):
        return {
            "id": self.id,
            "number": self.number,
            "code": self.code
        }


@event.listens_for(User, "before_insert")
def user_before_insert(mapper, conn, user):
    hasher = hashlib.sha256()
    hasher.update(str.encode(user.password))
    user.password = hasher.hexdigest()