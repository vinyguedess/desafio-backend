from setuptools import setup


setup(name="desafio-backend",
      author="Vinicius Guedes",
      author_email="viniciusgued@gmail.com",

      install_requires=[
            "flask", 
            "flask_restful",
            "flask_jwt_extended",
            "sqlalchemy",
            "flask_sqlalchemy"
      ],
      tests_require=[
            "flask_testing",
            "request"
      ])
